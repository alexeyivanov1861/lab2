#/bin/bash

HELPINFO="
usage: python_installer.sh [option]\n
\tOptions:\n
\t\t[none]			show this message\n
\t\t-h --help			show this message\n
\t\t-v VERSION			install Python<VERSION> in ~/soft/Python<VERSION> and creates link ~/bin/python<VERSION>
"

function install_python() {
	echo Installing python
	local version=$1
	cd ~/Downloads
	if [[ -f Python-$version.tar.xz ]]
	then
		echo Python-$version already downloaded
	else
		wget www.python.org/ftp/python/$version/Python-$version.tar.xz
	fi
	tar -xvf Python-$version.tar.xz
	cd Python-$version
	./configure --prefix=/gpfs/home/alivanov/soft/Python-$version
	make install
	ln -s /gpfs/home/alivanov/soft/Python-$version/bin/python3 /gpfs/home/alivanov/bin/python$version
	local path = [grep -P "export PATH=.*" ~/.bash_profile]
	if [ $path != "export PATH=$PATH:/gpfs/home/alivanov/bin" ]
	then
	       	echo "export PATH=$PATH:/gpfs/home/alivanov/bin"  | tee -a ~/.bash_profile
		export PATH=$PATH:/gpfs/home/alivanov/bin
	fi
	cd ..
}

function main() {
	if [[ $# -lt 2 ]] || [ $1 != "-v" ]
	then
		echo -e $HELPINFO
		exit 0
	fi
	install_python $2
}

main $@
