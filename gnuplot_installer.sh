#!/bin/bash

HELPINFO="
usage: gnuplot_installer.sh [option]\n
\tOptions:\n
\t\t[none]\n
\t\t\tshow this message\n
\t\t-h --help\n
\t\t\tshow this message\n
\t\tinstall <gnuplot_version> <libgd_version>\n
\t\t\tinstall gnuplot in ~/soft/gnuplot<VERSION> and creates link ~/bin/gnuplot-<VERSION>\n
"

function install_libgd() {
	echo Installing libgd
	local version=$1
	cd ~/Downloads
	if [[ -f libgd-$version.tar.xz ]]
	then
		echo libgd-$version already downloaded
	else
		wget "https://github.com/libgd/libgd/releases/download/gd-$version/libgd-$version.tar.xz" -O libgd-$version.tar.xz 
	fi
	tar -xvf libgd-$version.tar.xz
	cd libgd-$version
	./configure --prefix=/gpfs/home/alivanov/soft/libgd
	make install

	local path=`grep -P "export LD_LIBRARY_PATH=.*libgd/lib" ~/.bash_profile`
	if [[ -z $path  ]]
	then
		echo "LD_LIBRARY_PATH already exported"
	else
	       	echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/gpfs/home/alivanov/soft/libgd/lib" >> ~/.bash_profile
		export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/gpfs/home/alivanov/soft/libgd/lib
	fi
}

function install_gnuplot() {
	echo Installing gnuplot
	local version=$1
	cd ~/Downloads
	if [[ -f gnuplot-$version.tar.gz ]]
	then
		echo gnuplot-$version already downloaded
	else
		wget "https://sourceforge.net/projects/gnuplot/files/gnuplot/5.2.5/gnuplot-5.2.5.tar.gz/download?use_mirror=netcologne&r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fgnuplot%2Ffiles%2F&use_mirror=netcologne" -O gnuplot-$version.tar.gz --no-check-certificate
	fi
	tar -xvf gnuplot-$version.tar.gz
	cd gnuplot-$version

	./configure --with-gd=/gpfs/home/alivanov/soft/libgd -prefix=/gpfs/home/alivanov/soft/gnuplot-$version
	make install

	local path=`grep -P "export PATH=.*alivanov/bin" ~/.bash_profile`
	if [[ -z $path  ]]
	then
		echo "PATH already exported"
	else
	       	echo "export PATH=$PATH:/gpfs/home/alivanov/bin" >> ~/.bash_profile
		export PATH=$PATH:/gpfs/home/alivanov/bin
	fi
	ln -s ~/soft/gnuplot-$version/bin/gnuplot ~/bin/gnuplot-$version
}

function main() {
#$1 = Option $2 = gnuplot version $3 = libgd version
	if [[ $# -lt 3 ]] || [ $1 != "install" ]
	then
		echo -e $HELPINFO
		exit 0
	fi
	install_libgd $3
	install_gnuplot $2
}

main $@
